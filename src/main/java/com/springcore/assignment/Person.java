package com.springcore.assignment;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Person {
	
	@Value("101")
	private int id;
	@Value("Aniket")
	private String name;
	@Value("Student")
	private String desng;
	@Value("#{alist}")
	private List<String> contact;
	@Value("#{aset}")
	private Set<String> projects;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesng() {
		return desng;
	}
	public void setDesng(String desng) {
		this.desng = desng;
	}
	public List<String> getContact() {
		return contact;
	}
	public void setContact(List<String> contact) {
		this.contact = contact;
	}
	public Set<String> getProjects() {
		return projects;
	}
	public void setProjects(Set<String> projects) {
		this.projects = projects;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", desng=" + desng + ", contact=" + contact + ", projects="
				+ projects + "]";
	}
	

}
